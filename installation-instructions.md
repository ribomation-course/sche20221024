# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

* Zoom Client
    - https://us02web.zoom.us/download
    - [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* GIT Client
    - https://git-scm.com/downloads
* A BASH terminal, such as the GIT client terminal
* Node.js: `node` / `npm` / `npx` (Choose _Current_ or _LTS_)
    - https://nodejs.org/en/download/
* A decent IDE, such as
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
    * JetBrains WebStorm
        - https://www.jetbrains.com/webstorm/download
* A modern browser, such as any of
    - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
    - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
    - [Microsoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)

Test that you can create a fresh Vue 3 project using the command

    npx create vue@latest my-first-vue-app

_N.B.,_ if you have version 7.x of `npx`, change `create` to `init`, or
upgrade `npm`/`npx` to version 8.x.

Answer `No` to all questions and then

    cd my-first-vue-app
    npm install
    npm run dev


