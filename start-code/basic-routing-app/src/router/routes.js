import HomePage from '../pages/home-page.vue'


const routes = [
    {path: '/home', component: HomePage, meta: {label: 'Home'}},
    {path: '', redirect: '/home''},
]

export default routes;
