import {createRouter, createWebHistory} from 'vue-router'
import HomePage from '../pages/home-page.vue'
import ThanksPage from '../pages/thank-you.vue'


const routes = [
    {
        path: '/home', name: 'home', component: HomePage,
        meta: {
            title: 'Home - Router Demo',
            label: 'Home'
        }
    },

    {
        path: '/thank-you-very-much-for-your-registration',
        name: 'thanks',
        component: ThanksPage
    },

    {
        path: '/user-list', name: 'users', component: () => import('../pages/user-list.vue')
    },
    {
        path: '/user/:id', name: 'user', component: () => import('../pages/user.vue')
    },

    {path: '', redirect: '/home'}
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
})

export default router
