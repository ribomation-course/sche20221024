const url = 'https://randomuser.me/api/1.0/'

class UserService {
    async findUserById(id) {
        const res = await fetch(`${url}?seed=${id}`)
        if (res.ok) {
            const data = await res.json()
            const user = data.results[0]
            user.userId = id
            return user
        }
        console.error('http failed: %d %s', res.status, res.statusText)
        return undefined
    }

    async findAllUsersById(idList) {
        const lst = []
        for (let id of idList) {
            const user = await this.findUserById(id)
            lst.push(user)
        }
        return lst
    }

    intList(lb = 1, ub = 5) {
        const N = ub - lb + 1
        return Array.from(Array(N), (_, idx) => lb + idx)
    }
}

export default UserService

