import { createApp } from 'vue'
import App from './App.vue'
import './assets/style.css'

import IconWidget from './widgets/icon-widget.vue'
import ClockWidget from './widgets/clock-widget.vue'
const app = createApp(App)
app.component('Icon', IconWidget)    
app.component('Clock', ClockWidget)    
app.mount('#app')

