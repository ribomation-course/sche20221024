import HomePage from '../pages/home-page.vue'
import HelpPage from '../pages/help-page.vue'
import AboutPage from '../pages/about-page.vue'
import NotFoundPage from '../pages/not-found-page.vue'

const routes = [
    {path: '/home', component: HomePage},
    {path: '/help', component: HelpPage},
    {path: '/about', component: AboutPage},
    {path: '', redirect: '/home'},
    {path: '/:missingRoute(.*)', component: NotFoundPage}
];

export default routes;

