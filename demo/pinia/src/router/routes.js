import HomePage from '../pages/home.vue'
import EditPage from '../pages/edit.vue'


export const routes = [
    {path: '/home', name: 'home', component: HomePage},
    {path: '/edit/:id', name: 'edit', component: EditPage},

    {path: '', redirect: '/home'}
];

