import {ref, computed} from 'vue'
import {defineStore} from 'pinia'
import {DateTime} from 'luxon'

export class Todo {
    static nextId = 1

    constructor(what, when = new Date(), done = false) {
        this.what = what
        this.when = when
        this.done = done
        this.id = Todo.nextId++
    }

    toString() {
        return `Todo{${this.what}, ${this.when.toLocaleDateString()}, ${this.done ? 'DONE' : ''}`;
    }
}

export const useTodoStore = defineStore('todo', () => {
    const todos = ref([
        new Todo('Learn Vue', DateTime.now().minus({days: 2}).toJSDate(), true),
        new Todo('Build an app', DateTime.now().plus({days: 7}).toJSDate()),
        new Todo('Show it to the world', DateTime.now().plus({days: 12}).toJSDate()),
    ])

    function toggleDone(id) {
        id = Number(id)
        const idx = todos.value.findIndex(t => t.id === id)
        if (idx > -1) {
            const t = todos.value[idx]
            t.done = !t.done
            t.when = new Date()
        }
    }

    function remove(todo) {
        const idx = todos.value.findIndex(t => t.id === todo.id)
        if (idx > -1) {
            todos.value.splice(idx, 1)
        }
    }

    function replace(id, todo) {
        id = Number(id)
        const idx = todos.value.findIndex(t => t.id === id)
        if (idx > -1) {
            todos.value.splice(idx, 1, todo);
        } else {
            todos.value.push(todo)
        }
    }

    function all() { return [...todos.value]; }

    function one(id) {
        if (id === -1) {
            return new Todo('', new Date(), false)
        }
        return todos.value.find(t => t.id === id)
    }

    const todoCount  = computed(() => 
        todos.value
            .filter(t => !t.done)
            .length
    )

    return {
        todos, toggleDone, replace, remove, all, one, todoCount
    }
})


