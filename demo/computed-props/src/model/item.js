export class Item {
    constructor(data) {
        this.product = data.product
        this.price   = data.price
        this.count   = data.count
    }
    get amount() { return this.price * this.count }
}

