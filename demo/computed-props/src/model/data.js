export const jsonData = [
    {
        "product": "Salt And Pepper Mix - Black",
        "price": 316.57,
        "count": 1
    },
    {
        "product": "Scallops - 10/20",
        "price": 967.51,
        "count": 9
    }, {
        "product": "Lettuce - Red Leaf",
        "price": 560.81,
        "count": 10
    }, {
        "product": "Parsley - Dried",
        "price": 780.52,
        "count": 2
    }, {
        "product": "Pate - Cognac",
        "price": 938.51,
        "count": 10
    }, {
        "product": "Wine - Hardys Bankside Shiraz",
        "price": 1052.74,
        "count": 8
    }, {
        "product": "Muffin Hinge 117n",
        "price": 1641.63,
        "count": 5
    }, {
        "product": "Cranberry Foccacia",
        "price": 1364.21,
        "count": 10
    }, {
        "product": "Wine - Pinot Noir Mondavi Coastal",
        "price": 998.52,
        "count": 8
    }, {
        "product": "Bread - Frozen Basket Variety",
        "price": 985.03,
        "count": 5
    }
]

