import {createApp} from 'vue'
import App from './App.vue'

import './assets/style.css'

const app = createApp(App)

app.config.globalProperties.$pipes = {
    kr: (belopp, oren = true) => {
        const digits = oren ? 2 : 0
        const txt = Number(belopp).toLocaleString('sv', {
            useGrouping: true,
            minimumFractionDigits: digits,
            maximumFractionDigits: digits
        }) + ' kr'
        const NBSP = '\u00A0'
        return txt.replace(/\s+/g, NBSP)
    }
}

app.mount('#app')

