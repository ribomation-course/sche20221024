export class Todo {
    static nextId = 1;

    constructor(what, when, done=false) {
        this._id   = Todo.nextId++;
        this._what = what;
        this._when = when;
        this._done = done;
    }

    get id()      { return this._id; }
    get what()    { return this._what; }
    get when()    { return this._when; }
    get whenStr() { return this._when.toLocaleDateString(); }
    get done()    { return this._done; }

    toString() {
        return `Todo{id=${this.id}, ${this.what}, ${this.whenStr}, done=${this.done}}`;
    }

    toggle() {
        this._done = !this._done;
        return this._done;
    }
}

