import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

const app = createApp({
    data: () => ({
        name: 'Anna Conda',

        date: new Date().toLocaleString(),

        todos: [
            { what: 'Learn HTML', when: '2021-01-10', done: true },
            { what: 'Understand CSS', when: '2021-02-12', done: true },
            { what: 'Study JavaScript', when: '2021-03-14', done: true },
            { what: 'Investigate Vue.js', when: '2022-10-24', done: false },
            { what: 'Build an awesome app', when: '2021-11-15', done: false },
        ],

        newTodo: { what: '', when: new Date().toLocaleDateString(), done: false },
    }),

    methods: {
        toggleDone(todo) { todo.done = !todo.done },
        remove(idx) { this.todos.splice(idx, 1) },
        create() {
            const item = Object.assign({}, this.newTodo);
            this.todos.push(item);
            this.newTodo = { what: '', when: '', done: false }
        }
    }

});

app.mount('#app');
