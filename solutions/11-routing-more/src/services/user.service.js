const baseUrl = 'http://localhost:3000/users'

class UserService {
    async all() {
        try {
            const res = await fetch(baseUrl)
            if (res.ok) {
                return await res.json()
            }
            console.error('failed: %d %s', res.status, res.statusText)
        } catch (err) {
            console.error('failed: %o', err)
        }
        return undefined
    }

    async one(id) {
        try {
            const res = await fetch(`${baseUrl}/${id}`)
            if (res.ok) {
                return await res.json()
            }
            console.error('failed: %d %s', res.status, res.statusText)
        } catch (err) {
            console.error('failed: %o', err)
        }
        return undefined
    }
}

export default UserService

