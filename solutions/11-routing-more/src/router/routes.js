const UsersPage = () => import('../pages/user-list.vue');
const UserPage = () => import('../pages/user-detail.vue');

const routes = [
    {path: '/user-list', name: 'users', component: UsersPage},
    {path: '/user-detail/:id', name: 'user', component: UserPage},
    {path: '', redirect: {name: 'users'}},
]

export default routes;
