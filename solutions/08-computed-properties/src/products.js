export default [
  {
    "product": "Capon - Breast, Double, Wing On",
    "price": 1077.8,
    "count": 2
  },
  {
    "product": "Melon - Honey Dew",
    "price": 726.85,
    "count": 4
  },
  {
    "product": "Pastry - Key Limepoppy Seed Tea",
    "price": 463.75,
    "count": 10
  },
  {
    "product": "Island Oasis - Strawberry",
    "price": 1039.02,
    "count": 7
  },
  {
    "product": "Lid Tray - 16in Dome",
    "price": 991.37,
    "count": 1
  },
  {
    "product": "Cheese - Le Cru Du Clocher",
    "price": 1411.71,
    "count": 4
  },
  {
    "product": "Wine - Blue Nun Qualitatswein",
    "price": 1159.42,
    "count": 7
  },
  {
    "product": "Cup - 8oz Coffee Perforated",
    "price": 1447.72,
    "count": 9
  },
  {
    "product": "Water - Aquafina Vitamin",
    "price": 1340.78,
    "count": 2
  },
  {
    "product": "Wine - Chateau Aqueria Tavel",
    "price": 583.42,
    "count": 9
  }
]