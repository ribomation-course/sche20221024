import {createRouter, createWebHistory} from 'vue-router'
import ROUTES from './routes.js'
import AuthService from "../services/auth.service.js";


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: ROUTES
})

const authSvc = new AuthService()
const adminRoute = router.getRoutes().find(r => r.name === 'admin')
adminRoute.beforeEnter = (to, from) => {
    const ok = authSvc.isAuthenticated()
    if (ok) return true
    return {name: 'login', replace: true}
}

export default router
