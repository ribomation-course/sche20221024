const HomePage = () => import('../pages/home.page.vue');
const LoginPage = () => import('../pages/login.page.vue');
const AdminPage = () => import('../pages/admin.page.vue');

const routes = [
    {path: '/home', name: 'home', component: HomePage},
    {path: '/login', name: 'login', component: LoginPage},
    {path: '/admin', name: 'admin', component: AdminPage},
    {path: '', redirect: {name: 'home'}},
]

export default routes
