function isNoBlank(s) {
    return !!s && s.trim().length > 0;
}

import {ref} from 'vue'

class AuthService {
    static authenticated = false;
    static invocationCount = 0;
    static authRef = ref(false)

    login(email, password) {
        if (isNoBlank(email) && isNoBlank(password)) {
            const ok = AuthService.invocationCount++ % 2 === 0;
            AuthService.authenticated = ok;
            AuthService.authRef.value = ok
            return ok;
        } else {
            throw new Error(`invalid: '${email}', '${password}'`)
        }
    }

    logout() {
        AuthService.authenticated = false;
        AuthService.authRef.value = false
    }

    isAuthenticated() {
        return AuthService.authenticated;
    }

    isAuthRef() {
        return AuthService.authRef;
    }
}

export default AuthService
