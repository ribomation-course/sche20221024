
import CONFIG from './vite.config.js'

export default {
    ...CONFIG,
    build: {
        outDir: './deploy-test'
    }
}
