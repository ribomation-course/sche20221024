import HomePage from '../pages/home-page.vue'
import AboutPage from '../pages/about-page.vue'
import ProfilePage from '../pages/profile-page.vue'
import NotFoundPage from '../pages/not-found-page.vue'


const routes = [
    {path: '/home', component: HomePage, meta: {label: 'Home'}},
    {path: '/about', component: AboutPage, meta: {label: 'About...'}},
    {path: '/profile', component: ProfilePage, meta: {label: 'User Profile'}},
    {path: '', redirect: '/home'},
    {path: '/:any(.*)', component: NotFoundPage},
]

export default routes;
